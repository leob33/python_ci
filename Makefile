######################### CONFIG / FANCY STUFF #########################

ifneq ($(TERM),)
normal:=$(shell tput sgr0)
bold:=$(shell tput bold)
red:=$(shell tput setaf 1)
green:=$(shell tput setaf 2)
yellow:=$(shell tput setaf 3)
blue:=$(shell tput setaf 4)
purple:=$(shell tput setaf 5)
cyan:=$(shell tput setaf 6)
white:=$(shell tput setaf 7)
gray:=$(shell tput setaf 8)
endif

######################### USEFUL COMMANDS #########################

.PHONY: build-wheel install-wheel clean-project test-install clean-project install-dev-mode clean-venv build-from-source

build-from-source: build-wheel install-wheel clean-project test-install

build-wheel:
	pip install build
	python -m build --wheel

install-wheel:
	pip install dist/*

test-install:
	cli-addition

clean-project:
	rm -rf .mypy_cache .pytest_cache reports .coverage build dist demo_app.egg-info

install-dev-mode:
	pip install -e '.[dev]'

clean-venv:
	pip uninstall -y $(PACKAGE_NAME)
	pip freeze | xargs pip uninstall -y


######################### LOCAL CI #########################

DOCKER_IMAGE=python:3.10
COVERAGE=80
PROJECT_NAME=ci_python
PACKAGE_NAME=demo_app

.PHONY: install_dependencies check-source safety-check test-python run-ci-pipeline \
 run-ci-pipeline-with-dependency-installation run_ci_docker

install_dependencies:
	pip install -r requirements.txt
	pip install -r requirements_dev.txt

check-source:
	@echo "$(bold)$(blue) ================== flake8 ==================$(normal)"
	flake8 $(PACKAGE_NAME)
	@echo "$(bold)$(green)================== mypy ==================$(normal)"
	-mypy $(PACKAGE_NAME)
	@echo "$(bold)$(purple)================== vulture ==================$(normal)"
	-vulture $(PACKAGE_NAME)

safety-check:
	@echo "$(bold)$(yellow)================== safety ==================$(normal)"
	bandit $(PACKAGE_NAME)
	semgrep --config auto

test-python:
	@echo "$(bold)$(cyan)================== pytest ==================$(normal)"
	PYTHONPATH=. pytest --cov=$(PACKAGE_NAME) --cov-report=html:reports/pytest/cov_html --cov-fail-under=$(COVERAGE)

run-ci-pipeline: check-source safety-check test-python

run-ci-pipeline-with-dependency-installation: install_dependencies run-ci-pipeline

run-ci-docker:
	docker run -it --rm \
	-v $(PWD):/ci_python \
	--workdir /$(PROJECT_NAME) \
	$(DOCKER_IMAGE) \
	make run-ci-pipeline-with-dependency-installation

######################### BUILD PROJECT AS DOCKER IMAGE #########################

.PHONY: docker-build docker-image-rm docker-run-demo-app docker-run-demo-app

DOCKERFILE_IMAGE_PROJECT = dockerfiles/Dockerfile_dockerize_project
DOCKER_IMAGE_NAME = $(PACKAGE_NAME)

docker-image-rm:
	docker image rm -f $(DOCKER_IMAGE_NAME)

docker-build:
	docker build --file $(DOCKERFILE_IMAGE_PROJECT) -t $(DOCKER_IMAGE_NAME) .

# docker container test
docker-test:
	docker run -it --rm --entrypoint /bin/bash $(DOCKER_IMAGE_NAME)

docker-run-demo-app:
	docker run --rm $(DOCKER_IMAGE_NAME)

# NOTES : -t option fait que l'interaction avec docker ressemble à un terminal