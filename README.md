# Réaliser son pipeline de CI pour un projet python

## setup rapide : build package from source

Après avoir cloné le repo et s'être placé dans le repertoire du projet lancer la commande : 
``` make build-from-source```  qui va créer une archive wheel, l'installer, nettoyer le dossier 
du projet et tester l'application.<br>
Dans le terminal le message suivant devrait s'afficher : "The result is : 4"
Et c'est bon le package est installé

## Intro

Ce repo illustre l'article **lien** sur la construction d'un pipeline de CI en python avec le service
GitLab CI/CD.<br>
Le projet est structuré ainsi :<br>
- demo_app : un package python qui ne fait strictement rien, qui contient juste que code avec 
quelques erreurs afin de montrer ce que la CI est capable de détecter. <br>
- Makefile : un fichier contenant un ensemble de commandes utiles ainsi que les étapes de notre
CI pour la faire tourner en local<br>
- .gitlab-ci.yml : le fichier qui contient notre pipeline de CI qui sera lancé par GitLab CI/CD
- .flake8 et mypy.ini qui servent à personnaliser le comportement de flake8 et mypy

## Faire tourner la CI en local

Lancer ```make run-ci-pipeline-with-dependency-installation``` pour faire tourner le pipeline de CI
localement, avec notre shell comme environnement. 

Lancer ```make run-ci-docker``` pour faire tourner le pipeline de CI localement avec l'image Docker
spécifiée dans le Makefile comme environnement

Note : on remarquera que l'ajouter de '-' devant une commande sur le makefile permet de la rendre
optionnelle (ie si elle échoue la commande suivante est exécutée malgré tout)
